package sparktesting.repo;

public interface GreeterRepo {
    public Greeting insertGreeting(String name);
}
