package sparktesting.repo;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.local.server.DynamoDBProxyServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;
import static sparktesting.utils.DynamoUtils.*;
import static sparktesting.utils.Utils.getFreePort;

@RunWith(JUnit4.class)
public class DynamoDBGreeterRepoTest {
    DynamoDBProxyServer server = null;
    private Integer dynamoLocalPort = null;


    @Before
    public void startUp() throws Exception {
        dynamoLocalPort = getFreePort();
        this.server = dynamoServer(dynamoLocalPort);
        server.start();

        AmazonDynamoDB client = getDynamoClient(dynamoLocalPort);
        createGreetingTable(client);
    }

    @After
    public void tearDown() throws Exception {
        server.stop();
    }

    @Test
    public void test__insert_greeting_saves_to_dynamo() throws Exception {
        AmazonDynamoDBClient client = getDynamoClient(dynamoLocalPort);
        Greeting savedGreeting = new DynamoDBGreeterRepo(client).insertGreeting("myName");

        DynamoDBMapper mapper = new DynamoDBMapper(client);

        Greeting loaded = mapper.load(Greeting.class, savedGreeting.getId(), savedGreeting.getTimestamp());

        assertEquals(savedGreeting.getId(), loaded.getId());
        assertEquals(savedGreeting.getName(), loaded.getName());
        assertEquals(savedGreeting.getTimestamp(), loaded.getTimestamp());
    }

}
