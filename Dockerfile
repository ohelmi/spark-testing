FROM frekele/gradle:4.0-jdk8

ADD . /app

WORKDIR /app

RUN gradle shadowJar


EXPOSE 4567
CMD ["/usr/lib/jvm/java-8-openjdk-amd64/bin/java", "-jar", "build/libs/spark-testing-1.0-SNAPSHOT-all.jar"]
