package sparktesting.route;

import spark.Route;
import sparktesting.repo.GreeterRepo;

public class GreetingRouteBuilder {

    private final GreeterRepo greeterRepo;

    public GreetingRouteBuilder(GreeterRepo greeterRepo) {
        this.greeterRepo = greeterRepo;
    }

    public Route buildHelloRoute() {
        return (req, res) -> {
            String name = req.queryParams("name");
            greeterRepo.insertGreeting(name);
            return "hello " + name;
        };
    }
}
