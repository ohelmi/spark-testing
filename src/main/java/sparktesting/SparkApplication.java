package sparktesting;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import sparktesting.repo.DynamoDBGreeterRepo;
import sparktesting.route.GreetingRouteBuilder;

import static spark.Spark.get;
import static spark.Spark.port;

public class SparkApplication {
  private static final int DEFAULT_PORT = 8080;
  public static void main(String[] args) {
    final AmazonDynamoDBClient dynamoLocalClient = (AmazonDynamoDBClient) AmazonDynamoDBClient.builder()
              .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration("http://localhost:8000", "eu-west-1"))
              .build();

    startServer(DEFAULT_PORT, dynamoLocalClient);
  }

  public static void startServer(int port, AmazonDynamoDBClient client){
    final GreetingRouteBuilder greetingBuilder = new GreetingRouteBuilder(new DynamoDBGreeterRepo(client));
    port(port);
    get("/hello", greetingBuilder.buildHelloRoute());
    get("/status", (req, res) -> "spark-testing");
  }

}

