package sparktesting;

import akka.actor.ActorSystem;
import akka.http.javadsl.Http;
import akka.http.javadsl.model.HttpResponse;
import akka.http.scaladsl.model.HttpRequest;
import akka.stream.ActorMaterializer;
import akka.util.ByteString;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.local.server.DynamoDBProxyServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import spark.Spark;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.List;
import java.util.concurrent.CompletionStage;

import static jdk.nashorn.internal.objects.Global.println;
import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;
import static sparktesting.utils.DynamoUtils.createGreetingTable;
import static sparktesting.utils.DynamoUtils.dynamoServer;
import static sparktesting.utils.DynamoUtils.getDynamoClient;
import static sparktesting.utils.Utils.getFreePort;


@RunWith(JUnit4.class)
public class DeploymentTest {

    private Integer port = null;
    private Integer dynamoLocalPort = null;
    DynamoDBProxyServer server = null;


    @Before
    public void setUp() throws Exception{
        port = getFreePort();
        dynamoLocalPort = getFreePort();
        server = dynamoServer(dynamoLocalPort);
        server.start();

        AmazonDynamoDBClient dynamoClient = getDynamoClient(dynamoLocalPort);
        createGreetingTable(dynamoClient);

        SparkApplication.startServer(port, dynamoClient);
    }

    @After
    public void tearDown() throws Exception{
        Spark.stop();
        server.stop();
    }

    @Test
    public void test__hello_endpoint_render_hello_name_param() {
        final ActorSystem system = ActorSystem.create();
        ActorMaterializer materializer = ActorMaterializer.create(system);
        String uri = "http://localhost:" + port + "/hello?name=Alex";
        final CompletionStage<HttpResponse> response = Http.get(system)
                .singleRequest(HttpRequest.create(uri), materializer);

        assertResponse(materializer, response, "hello Alex");
    }

    @Test
    public void test__status_endpoint_responds_with_service_name() {
        final ActorSystem system = ActorSystem.create();
        ActorMaterializer materializer = ActorMaterializer.create(system);
        String uri = "http://localhost:" + port + "/status";
        final CompletionStage<HttpResponse> response = Http.get(system)
                .singleRequest(HttpRequest.create(uri), materializer);
        assertResponse(materializer, response, "spark-testing");
    }

    private void assertResponse(ActorMaterializer materializer, CompletionStage<HttpResponse> response, Object expectedResult) {
        try {
            final String responseString = response.thenCompose((resp) -> resp.entity().getDataBytes()
                    .runFold(ByteString.empty(), ByteString::concat, materializer))
                    .toCompletableFuture().get().utf8String();
            System.out.println(responseString);
            System.out.println(expectedResult);
            assertEquals(expectedResult, responseString);
        } catch (Exception e) {
            fail("Exception " + e.getMessage() + " thrown");
        }
    }
}
