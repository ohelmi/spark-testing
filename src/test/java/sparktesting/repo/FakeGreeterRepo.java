package sparktesting.repo;

public class FakeGreeterRepo  implements GreeterRepo {
    private Greeting greeting = null;

    public FakeGreeterRepo(){

    }

    public FakeGreeterRepo(Greeting greeting) {
        this.greeting = greeting;
    }

    @Override
    public Greeting insertGreeting(String name) {
        greeting = Greeting.builder()
                .withId("id-1")
                .withName(name)
                .withTimestamp(100L)
                .build();
        return greeting;
    }

    public Greeting getGreeting(){
        return greeting;
    }
}
