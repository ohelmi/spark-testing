package sparktesting.route;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import spark.Request;
import spark.Response;
import sparktesting.repo.FakeGreeterRepo;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class GreetingRouteBuilderTest {

    @Test
    public void test__hello_endpoint_saves_each_greeting() throws Exception {
        FakeGreeterRepo greeterRepo = new FakeGreeterRepo();
        GreetingRouteBuilder builder = new GreetingRouteBuilder(greeterRepo);
        Request req = mock(Request.class);
        Response res = mock(Response.class);
        when(req.queryParams("name")).thenReturn("some name");

        builder.buildHelloRoute().handle(req, res);

        assertNotNull(greeterRepo.getGreeting());
    }
}
