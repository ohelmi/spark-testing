package sparktesting.utils;

import java.io.IOException;
import java.net.ServerSocket;

public class Utils {
    public static int getFreePort() throws IOException {
        ServerSocket sock = new ServerSocket(0);
        int freePort = sock.getLocalPort();
        sock.close();

        return freePort;
    }

}
