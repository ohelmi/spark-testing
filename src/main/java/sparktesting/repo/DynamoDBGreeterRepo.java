package sparktesting.repo;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.*;

import java.util.UUID;

public class DynamoDBGreeterRepo implements GreeterRepo {

    private final DynamoDBMapper mapper;

    public DynamoDBGreeterRepo(AmazonDynamoDBClient client){
        mapper = new DynamoDBMapper(client);
    }


    public Greeting insertGreeting(String name) {
        final Greeting greeting = Greeting.builder()
                .withId(UUID.randomUUID().toString())
                .withName(name)
                .withTimestamp(System.currentTimeMillis())
                .build();
        mapper.save(greeting);
        return greeting;
    }

}
