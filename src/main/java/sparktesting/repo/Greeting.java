package sparktesting.repo;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "Greetings")
public class Greeting{
    private String id;
    private String name;
    private Long timestamp;

    private Greeting(String id, String name, long timestamp){
        this.id = id;
        this.name = name;
        this.timestamp = timestamp;
    }

    public Greeting(){

    }

    @DynamoDBHashKey(attributeName = "Id")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @DynamoDBAttribute(attributeName = "Name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @DynamoDBRangeKey(attributeName = "Timestamp")
    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }
    public static Builder builder(){
        return new Builder();
    }
    public static class Builder{
        private String id;
        private String name;
        private Long timestamp;
        public Builder withId(String id){
            this.id = id;
            return this;
        }
        public Builder withName(String name){
            this.name = name;
            return this;
        }
        public Builder withTimestamp(long timestamp){
            this.timestamp = timestamp;
            return this;
        }
        public Greeting build(){
            return new Greeting(id, name, timestamp);
        }
    }
}