package sparktesting.utils;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.local.main.ServerRunner;
import com.amazonaws.services.dynamodbv2.local.server.DynamoDBProxyServer;
import com.amazonaws.services.dynamodbv2.model.*;
import com.google.common.collect.ImmutableList;
import scala.Tuple3;

import java.util.List;
import java.util.stream.Collectors;

public class DynamoUtils {

    public static DynamoDBProxyServer dynamoServer(int port) throws Exception {
        final String[] localArgs = {"-inMemory", "-port", port+""};
        return ServerRunner.createServerFromCommandLineArgs(localArgs);
    }

    public static AmazonDynamoDBClient getDynamoClient(int port) {
        return (AmazonDynamoDBClient) AmazonDynamoDBClient.builder()
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration("http://localhost:"+port, "eu-west-1"))
                .build();

    }

    public static CreateTableRequest createTableRequest(String tableName, List<Tuple3<String, KeyType, ScalarAttributeType>> keysSchema) {
        List<KeySchemaElement> keySchema = keysSchema.stream().map(
                info -> new KeySchemaElement()
                        .withAttributeName(info._1()).withKeyType(info._2())
        ).collect(Collectors.toList());

        List<AttributeDefinition> attributeDefinitions = keysSchema.stream().map(
                info -> new AttributeDefinition()
                        .withAttributeName(info._1()).withAttributeType(info._3())
        ).collect(Collectors.toList());

        return new CreateTableRequest()
                .withTableName(tableName)
                .withKeySchema(keySchema)
                .withAttributeDefinitions(attributeDefinitions)
                .withProvisionedThroughput(new ProvisionedThroughput(5L, 5L));
    }

    public static void createGreetingTable (AmazonDynamoDB client){
        final String partitionKeyName = "Id";
        final String tableName = "Greetings";

        List<Tuple3<String, KeyType, ScalarAttributeType>> keysSchema = ImmutableList.of(
                Tuple3.apply(partitionKeyName, KeyType.HASH, ScalarAttributeType.S),
                Tuple3.apply("Timestamp", KeyType.RANGE, ScalarAttributeType.N)
        );
        CreateTableRequest tableRequest = createTableRequest(tableName, keysSchema);
        client.createTable(tableRequest);
    }

}
